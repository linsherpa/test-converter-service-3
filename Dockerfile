From ubuntu:latest
RUN apt-get update && \
    apt-get -qy full-upgrade && \
    apt-get -y install systemd && \
    apt-get install -qy curl && \
    apt-get install -y nodejs && \
    apt-get install -y docker.io && \
    apt-get install -y python3-pip && \
    pip3 install cwltool
