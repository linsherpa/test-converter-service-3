import os
import shutil

## Requirements
## The folders: intermediate_files, mzml_conversion, mzml_validation are created beforehand
##

## Setting the folder to the working Directory

main_dir = os.getcwd()
sub_dir = "input_files"
int_dir = "intermediate_files_"
work_path = os.path.join(main_dir, sub_dir)
int_path = os.path.join(main_dir, int_dir)
os.chdir(main_dir)

class preparation:
    def __int__(self):
        self.root = None

    def cleanup_files(self):
        for file in os.listdir(int_path):
            if os.path.isfile(os.path.join(int_path, file)):
                os.remove(os.path.join(int_path, file))
            if os.path.isdir(os.path.join(int_path, file)):
                shutil.rmtree(os.path.join(int_path, file))

        for file in os.listdir(work_path):
            if os.path.isfile(os.path.join(work_path, file)):
                os.remove(os.path.join(work_path, file))
            if os.path.isdir(os.path.join(work_path, file)):
                shutil.rmtree(os.path.join(work_path, file))
        os.chdir(main_dir)

    def create_intermediate_files(self):
        os.chdir(work_path)
        # creating an intermediate metadata files for junit report
        for file in os.listdir(os.getcwd()):
            if (os.path.isfile(file) or os.path.isdir(file)) and file!= ".gitkeep" and ".wiff.scan" not in file.lower():
                tmp_file_basename = os.path.splitext(file)[0]
                os.chdir(main_dir)
                #create junit report for mzml conversion
                tmp_intermediate_file = int_dir + "/msconvert_workflow_file_" + tmp_file_basename + ".yml"
                with open(tmp_intermediate_file, 'w') as g:
                    g.write('in_file:\n    class: File\n    format: http://edamontology.org/format_3245\n')
                    g.write(f'    path: {os.path.join(work_path, file)}')
                print(f'The Intermediate file Completed: {file} ')
                os.chdir(work_path)

            elif ".wiff.scan" in file:
                print("Secondary-file for the File found!")
        os.chdir(main_dir)


#if __name__ == "__main__":
    #test = preparation()
    #test.create_intermediate_files()
    #os.chdir(main_dir)