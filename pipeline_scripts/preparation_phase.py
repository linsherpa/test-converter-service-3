import os

## Requirements
## The folders: intermediate_files, mzml_conversion, mzml_validation are created beforehand
##

## Setting the folder to the working Directory

os.path.dirname(os.getcwd())
directory_names = ["MS+Thermo_Finnigan", "MS+Thermo_Scientific", "MS+Agilent_MassHunter_D", "MS+Sciex_WIFF"]
main_dir = os.getcwd()
sub_dir = "example_file"
int_dir = "intermediate_files"
work_path = os.path.join(main_dir, sub_dir)
os.chdir(work_path)


class preparation:
    def __int__(self):
        self.root = None

    def create_intermediate_files(self):
        os.chdir(work_path)
        for fld in directory_names:
            print("-----------------------")
            print(fld)
            print("-----------------------")
            if fld in os.listdir(os.getcwd()):
                print(f'Working on the instrument "{fld}"::')
                os.chdir(fld)
                # creating an intermediate metadata files for junit report
                for file in os.listdir(os.getcwd()):
                    if (os.path.isfile(file) or os.path.isdir(file)) and file!= ".gitkeep" and ".wiff.scan" not in file.lower():
                        tmp_file_basename = os.path.splitext(file)[0]
                        tmp_file_name = tmp_file_basename + ".mzML"
                        os.chdir(os.path.join(main_dir, sub_dir))
                        report_conversion = "mzml_conversion/" + tmp_file_basename + "_mzML_conversion_op.txt"
                        report_validation = "mzml_validation/" + tmp_file_basename + "_mzML_validation_op.txt"

                        with open(report_conversion, 'w') as f:
                            f.write(file)

                        with open(report_validation, 'w') as f:
                            f.write(tmp_file_name)
                        #print(os.getcwd())
                        os.chdir(main_dir)
                        #create junit report for mzml conversion
                        tmp_intermediate_file = "intermediate_files/msconvert_workflow_file_" + tmp_file_basename + ".yml"
                        file_location = os.path.join(work_path, fld)
                        with open(tmp_intermediate_file, 'w') as g:
                            g.write('in_file:\n    class: File\n    format: http://edamontology.org/format_3245\n')
                            g.write(f'    path: {os.path.join(file_location, file)}')
                        print(f'The Intermediate file Completed: {file} ')
                        os.chdir(os.path.join(work_path, fld))
                        #print(f'The name of the file is {os.path.splitext(file)[0]}')
                    else:
                        print(f'Skipping the file: {file}')
            else:
                print("No files found in the registered list")
            os.chdir(work_path)


test = preparation()
test.create_intermediate_files()
os.chdir(main_dir)
